package frombooks.javaruszglowa.zgadywanka;

public class Zgadywanka {
    Gracz player1;
    Gracz player2;
    Gracz player3;

    public void rozpocznijGre() {
        player1 = new Gracz();
        player2 = new Gracz();
        player3 = new Gracz();

        int tryPlayer1 = 0;
        int tryPlayer2 = 0;
        int tryPlayer3 = 0;

        boolean player1Win = false;
        boolean player2Win = false;
        boolean player3Win = false;

        int numberToFind = (int) (Math.random() * 10);
        System.out.println("Myślę o liczbie z zakresu od 0 do 9");

        while (true) {
            System.out.println("Myślę o liczbie: " + numberToFind);

            player1.zgaduj();
            player2.zgaduj();
            player3.zgaduj();

            tryPlayer1 = player1.liczba;
            System.out.println("Gracz pierwszy wytypował liczbę: " + tryPlayer1);
            tryPlayer2 = player2.liczba;
            System.out.println("Gracz drugi wytypował liczbę: " + tryPlayer2);
            tryPlayer3 = player3.liczba;
            System.out.println("Gracz trzeci wytypował liczbę: " + tryPlayer3);

            if (tryPlayer1 == numberToFind) {
                player1Win = true;
            }
            if (tryPlayer2 == numberToFind) {
                player2Win = true;
            }
            if (tryPlayer3 == numberToFind){
                player3Win = true;
            }

            if(player1Win || player2Win || player3Win){
                System.out.println("Mamy zwycięzcę!");
                System.out.println("Czy wygrał gracz nr 1: "+ player1Win);
                System.out.println("Czy wygrał gracz nr 2: "+ player2Win);
                System.out.println("Czy wygrał gracz nr 3: "+ player3Win);
                System.out.println("Koniec gry");
                break; //koniec pętli i działania metody rozpocznijGre

            }else{
                System.out.println("Każdy gracz ma następną próbę");
            }   //koniec petli if else

        }       //koniec while
    }           //koniec metody rozpocznijGre
}               //koniec klasy
