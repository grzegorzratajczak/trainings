package fullcourse.zadania003.ex03;

public class Reverser {

    public String reverseString(String stringFromUser){

        String revString = new StringBuilder(stringFromUser).reverse().toString();
        return revString;

    }
}
