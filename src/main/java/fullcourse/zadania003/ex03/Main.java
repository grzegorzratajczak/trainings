package fullcourse.zadania003.ex03;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Podaj wyrażenie do przekształcenia: ");
        Scanner scanner = new Scanner(System.in);

        String phraseFromUser = scanner.nextLine();
        Reverser reverser = new Reverser();

        System.out.println("Odwrocona fraza: " + reverser.reverseString(phraseFromUser));

    }
}
