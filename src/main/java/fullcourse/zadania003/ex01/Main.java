package fullcourse.zadania003.ex01;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        System.out.println("Wprowadź liczbę: ");
        System.out.println("Twoja liczba jest " + (((new Scanner(System.in).nextInt() & 1) == 0) ? "parzysta" : "nie parzysta"));
    }
}
