package fullcourse.zadania003.ex2;

public class PrimeValidator {
    //checks whether an int is prime or not.
    boolean isPrime(int number) {

        if (number == 1) {
            return false;
        }

        if (number == 2) {
            return true;
        }
        //check if number is a multiple of 2

        if (number % 2 == 0) {
            return false;
        }
        //if not, then just check the odds

        for (int i = 3; i * i <= number; i += 2) {
            if (number % i == 0)
                return false;
        }
        return true;
    }
}
