package fullcourse.zadania003.ex2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Podaj liczbę do sprawdzenia; ");
        PrimeValidator validator = new PrimeValidator();
        int scanner = new Scanner(System.in).nextInt();

        boolean isValid = validator.isPrime(scanner);

        System.out.println("Podana liczba " + scanner + (isValid ? " jest liczbą pierwszą" : " nie jest liczbą pierwszą") );

    }
}
