package fullcourse.zadania003.ex04;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Podaj wyrażenie; ");
        Scanner scanner = new Scanner(System.in);

        String stringFromConsole = scanner.nextLine();

        DictionaryChanger changer = new DictionaryChanger();

        String changeString = changer.changePolishChar(stringFromConsole);

        System.out.println("Twoja przekształcona fraza: " + changeString);
    }
}
