package fullcourse.zadania003.ex04;

public class DictionaryChanger {

    public String changePolishChar(String stringFromUser){

        stringFromUser = stringFromUser.replace('ą', 'a').replace('Ą', 'A');
        stringFromUser = stringFromUser.replace('ć', 'c').replace('Ć', 'ć');
        stringFromUser = stringFromUser.replace('ę', 'e').replace('Ę', 'E');
        stringFromUser = stringFromUser.replace('ł', 'l').replace('Ł', 'L');
        stringFromUser = stringFromUser.replace('ń', 'n').replace('Ń', 'N');
        stringFromUser = stringFromUser.replace('ó', 'o').replace('Ó', 'O');
        stringFromUser = stringFromUser.replace('ś', 's').replace('Ś', 'S');
        stringFromUser = stringFromUser.replace('ź', 'z').replace('Ź', 'Z');
        stringFromUser = stringFromUser.replace('ż', 'z').replace('Ż', 'Z');

        return stringFromUser;
    }

}
