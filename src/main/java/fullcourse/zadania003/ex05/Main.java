package fullcourse.zadania003.ex05;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Podaj palindrom: ");
        Scanner scanner = new Scanner(System.in);
        String word = scanner.nextLine();
        String word2 = word.replace(" ", "");
        String wordToCheck = word2.toLowerCase();
        char[] warray = wordToCheck.toCharArray();

        PalindromeCheck check = new PalindromeCheck();

        System.out.printf("Wprowadzony ciąg znaków: \"" + word + "\"%s jest palindromem", check.itIsPalindrom(warray) ? "" : " nie");

    }
}
