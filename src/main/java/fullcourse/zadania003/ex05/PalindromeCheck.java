package fullcourse.zadania003.ex05;

public class PalindromeCheck {

    public static boolean itIsPalindrom(char[] word) {

        boolean palindrom = false;

        if (word.length % 2 == 0) {
            for (int i = 0; i < word.length / 2 - 1; i++) {
                if (word[i] != word[word.length - i - 1]) {
                    return false;
                } else {
                    palindrom = true;
                }
            }
        } else {
            for (int i = 0; i < (word.length - 1) / 2 - 1; i++) {
                if (word[i] != word[word.length - i - 1]) {
                    return false;
                } else {
                    palindrom = true;
                }
            }
        }
        return palindrom;
    }

}
