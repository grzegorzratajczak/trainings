package fullcourse.enums;

public enum Size {
    L(1) {
        @Override
        public boolean isLarge() {
            return true;
        }
    },
    M(2),
    S(3){
        @Override
        public boolean isSmall(){
            return true;
        }
    },;

    private int size;

    public boolean isLarge() {
        return false;
    }

    public boolean isSmall() {
        return false;
    }

    Size(int size) {
        this.size = size;
    }
}
