package fullcourse.enums.myExample;

public enum Sex {
    M("Mężczyzna"){
        @Override
        public boolean isMale(){
            return true;
        }
    },
    F("Kobieta"),;

    private String sex;

    Sex(String sex) {
        this.sex = sex;
    }

    public String getSex() {
        return sex;
    }

    public boolean isMale(){
        return false;
    }
}
