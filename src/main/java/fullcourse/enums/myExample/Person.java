package fullcourse.enums.myExample;

public class Person {

    private String name;
    private String surName;
    private Sex sexForPerson;
    private EyesColor eyesColorForPerson;

    public Person(String name, String surName, Sex sexForPerson, EyesColor eyesColorForPerson) {
        this.name = name;
        this.surName = surName;
        this.sexForPerson = sexForPerson;
        this.eyesColorForPerson = eyesColorForPerson;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public Sex getSexForPerson() {
        return sexForPerson;
    }

    public void setSexForPerson(Sex sexForPerson) {
        this.sexForPerson = sexForPerson;
    }

    public EyesColor getEyesColorForPerson() {
        return eyesColorForPerson;
    }

    public void setEyesColorForPerson(EyesColor eyesColorForPerson) {
        this.eyesColorForPerson = eyesColorForPerson;
    }

    @Override
    public String toString() {
        String toShow = ("\r\n" + getName()
                + " " + getSurName()
                + " " + "Płeć: " + getSexForPerson().getSex()
                + " " + "Oczy: " + getEyesColorForPerson());
        return toShow;
    }
}
