package fullcourse.enums.myExample;

public enum EyesColor {
    Niebieskie,
    Brązowe,
    Zielone,
    Czarne,
    Szare,
    Piwne,
}
