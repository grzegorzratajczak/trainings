package fullcourse.enums.myExample;

import java.util.ArrayList;
import java.util.List;

public class myExampleStart {
    public static void main(String[] args) {

        Person person1 = new Person("Jan", "Wieśniak", Sex.M, EyesColor.Piwne);
        Person person2 = new Person("Kazio", "Hulak", Sex.M, EyesColor.Czarne);
        Person person3 = new Person("Wiesława", "Donosi", Sex.F, EyesColor.Niebieskie);
        Person person4 = new Person("Ania", "Frania", Sex.F, EyesColor.Czarne);
        Person person5 = new Person("Kunegunda", "Folk", Sex.F, EyesColor.Szare);

        List<Person> listOfPerson = new ArrayList<>();
        listOfPerson.add(person1);
        listOfPerson.add(person2);
        listOfPerson.add(person3);
        listOfPerson.add(person4);
        listOfPerson.add(person5);

        System.out.println(listOfPerson.toString());

    }
}
