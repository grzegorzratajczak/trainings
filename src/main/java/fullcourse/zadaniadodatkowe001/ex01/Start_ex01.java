package fullcourse.zadaniadodatkowe001.ex01;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Button;


public class Start_ex01 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {

        primaryStage.show();
        Engine myGame = new Engine();

        BorderPane layout = new BorderPane();

        Scene scene = new Scene(layout, 400, 200);

        primaryStage.setScene(scene);
        primaryStage.setTitle("Zgadywanka liczby");

        TextField textField = new TextField();
        Label label = new Label("Wciśnij START żeby zacząć");

        Button buttonCheck = new Button("Sprawdzam");
        buttonCheck.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                label.setText("Naliczam próbę, jeśli nie podałeś liczby również ;)");
                myGame.player.setPlayerBidCounter(myGame.player.getPlayerBidCounter() + 1);
                myGame.player.setPlayerBid(Integer.parseInt(textField.getText()));
                if (myGame.checkBid()) {
                    label.setText("Zgadłeś! Twoja liczba prób: " + myGame.player.getPlayerBidCounter());
                } else {
                    if (myGame.isLower()) {
                        label.setText("Liczba której szukasz jest mniejsza, podałeś: " + myGame.player.getPlayerBid());
                    } else {
                        label.setText("Liczba której szukasz jest większa, podałeś: " + myGame.player.getPlayerBid());
                    }
                }

            }
        });

        Button buttonStart = new Button("START");
        buttonStart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                label.setText("Rozpoczynamy grę!");
                myGame.startGame();
                textField.setText("Zgadnij liczbę od 1 do 100 " + "(" + myGame.numberToFind + ")");
            }
        });

        VBox vBox = new VBox();
        vBox.getChildren().addAll(buttonStart, textField, buttonCheck, label);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(10);

        layout.setCenter(vBox);

    }
}
