package fullcourse.zadaniadodatkowe001.ex01;

public class Player {

    private int playerBid;
    private int playerBidCounter;

    public Player(int playerBid, int playerBidCounter) {
        this.playerBid = playerBid;
        this.playerBidCounter = playerBidCounter;
    }

    public int getPlayerBid() {
        return playerBid;
    }

    public void setPlayerBid(int playerBid) {
        this.playerBid = playerBid;
    }

    public int getPlayerBidCounter() {
        return playerBidCounter;
    }

    public void setPlayerBidCounter(int playerBidCounter) {
        this.playerBidCounter = playerBidCounter;
    }
}
