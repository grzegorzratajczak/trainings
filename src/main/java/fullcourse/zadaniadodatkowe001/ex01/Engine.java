package fullcourse.zadaniadodatkowe001.ex01;

public class Engine {

    Player player = new Player(0, 0);
    int numberToFind = 999;

    public void numberDraw() {
        numberToFind = (int) (Math.random() * 100) + 1;
    }

    public void startGame() {
        player.setPlayerBid(0);
        player.setPlayerBidCounter(0);
        numberDraw();
    }

    public boolean checkBid() {
        boolean bool;
        if (numberToFind == player.getPlayerBid()) {
            bool = true;
        } else {
            bool = false;
        }
        return bool;
    }

    public boolean isLower() {
        boolean bool;
        if (numberToFind < player.getPlayerBid()) {
            bool = true;
        } else {
            bool = false;
        }
        return bool;
    }

}
