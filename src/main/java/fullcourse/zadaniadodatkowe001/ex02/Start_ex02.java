package fullcourse.zadaniadodatkowe001.ex02;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Button;

public class Start_ex02 extends Application {
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.show();
        BorderPane layout = new BorderPane();
        Scene scene = new Scene(layout, 600, 400);
        primaryStage.setScene(scene);
        primaryStage.setTitle("Zgadywanka001 - Ex02");
        SystemLogic_ex02 gameLogic = new SystemLogic_ex02();

        TextField textField = new TextField();
        Label label1 = new Label("Wciśnij start żeby zacząć");
        Label label2 = new Label("Tutaj będę informował czy zgadłeś i która to próba");

        Button buttonStart = new Button("START");
        buttonStart.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                gameLogic.startGame();
                gameLogic.nextRound();
                label1.setText("Podaj sumę liczb: " + gameLogic.number1 + " + " + gameLogic.number2);
            }
        });

        Button buttonCheck = new Button("Sprawdź");
        buttonCheck.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                gameLogic.gamePlayer.setPlayerTry(Integer.parseInt(textField.getText()));
                gameLogic.checkSum(gameLogic.gamePlayer.getPlayerTry());
                label2.setText("Podałeś: " + gameLogic.gamePlayer.getPlayerTry()
                        + " Szukałeś: " + gameLogic.sumOfNumbers
                        + " Błędy: " + gameLogic.gamePlayer.getBadTry()
                        + " Poprawne: " + gameLogic.gamePlayer.getGoodTry()
                        + " Proba nr: " + gameLogic.counterOfTrys);
                gameLogic.nextRound();
                label1.setText("Podaj sumę liczb: " + gameLogic.number1 + " + " + gameLogic.number2);
            }
        });

        VBox vBox = new VBox();
        vBox.getChildren().addAll(buttonStart, label1, textField, buttonCheck, label2);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(15);
        layout.setCenter(vBox);
    }
}
