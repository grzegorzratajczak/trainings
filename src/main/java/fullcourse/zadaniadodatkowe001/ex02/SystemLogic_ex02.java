package fullcourse.zadaniadodatkowe001.ex02;

import java.util.Random;

public class SystemLogic_ex02 {

    Player gamePlayer = new Player(99, 0, 0, null);
    int number1;
    int number2;
    int sumOfNumbers;
    int counterOfTrys;

    public void numberDraw() {
        Random random = new Random();
        number1 = random.nextInt(51);
        number2 = random.nextInt(51);
        sumOfNumbers = number1 + number2;
    }

    public void resetPlayer(Player player) {
        player.setPlayerTry(99);
        player.setBadTry(0);
        player.setGoodTry(0);
        player.setTrysTime(null);
    }

    public void startGame() {
        counterOfTrys = 0;
        resetPlayer(gamePlayer);
    }

    public void nextRound() {
        if (counterOfTrys < 5) {
            counterOfTrys++;
            numberDraw();
        } else {
            endGame();
        }
    }

    public void checkSum(int sumFromPlayer) {
        if (sumOfNumbers == sumFromPlayer) {
            gamePlayer.setGoodTry(gamePlayer.getGoodTry() + 1);
        } else {
            gamePlayer.setBadTry(gamePlayer.getBadTry() + 1);
        }
    }

    public void endGame() {
        return;
    }
}
