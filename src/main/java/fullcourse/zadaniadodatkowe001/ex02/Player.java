package fullcourse.zadaniadodatkowe001.ex02;

public class Player {

    private int playerTry;
    private int goodTry;
    private int badTry;
    private long[] trysTime;

    public Player(int playerTry, int goodTry, int badTry, long[] trysTime) {
        this.playerTry = playerTry;
        this.goodTry = goodTry;
        this.badTry = badTry;
        this.trysTime = trysTime;
    }

    public int getPlayerTry() {
        return playerTry;
    }

    public void setPlayerTry(int playerTry) {
        this.playerTry = playerTry;
    }

    public int getGoodTry() {
        return goodTry;
    }

    public void setGoodTry(int goodTry) {
        this.goodTry = goodTry;
    }

    public int getBadTry() {
        return badTry;
    }

    public void setBadTry(int badTry) {
        this.badTry = badTry;
    }

    public long[] getTrysTime() {
        return trysTime;
    }

    public void setTrysTime(long[] trysTime) {
        this.trysTime = trysTime;
    }
}
