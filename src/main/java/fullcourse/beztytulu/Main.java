package fullcourse.beztytulu;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        int[] tableToSort = new int[]{8,7,5,4,2,6,9,5,5};

        System.out.println(Arrays.toString(tableToSort));

        SortBubble sortBubble = new SortBubble();

        int[] sortedByBubble = sortBubble.bubbleSort(tableToSort);

        System.out.println(Arrays.toString(sortedByBubble));

        SortCount sortCount = new SortCount();

        int[] sortedByCount = sortCount.sortByCount(tableToSort);
        System.out.println(Arrays.toString(sortedByCount));
    }
}
