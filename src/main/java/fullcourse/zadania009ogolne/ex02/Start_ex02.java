package fullcourse.zadania009ogolne.ex02;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Start_ex02 extends Application{
   FindNearestPrime findNearestPrime = new FindNearestPrime();

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.show();
        BorderPane layout = new BorderPane();
        Scene scene = new Scene(layout);
        primaryStage.setScene(scene);
        Label label = new Label("Wpisz cos w pole tekstowe");
        TextField textField = new TextField("Tutaj wpisz wartość do sprawdzenia");
        Button checkButton= new Button("Sprawdź");

        checkButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Integer toShow = findNearestPrime.findPrime(Integer.parseInt(textField.getText()));
                label.setText(String.format("%s", toShow));
            }
        });


        VBox vBox = new VBox();
        vBox.getChildren().addAll(textField, checkButton, label);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(15);
        layout.setCenter(vBox);
    }
}
