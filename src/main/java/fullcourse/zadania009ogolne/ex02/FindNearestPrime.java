package fullcourse.zadania009ogolne.ex02;

public class FindNearestPrime {

    PrimeChecker primeChecker = new PrimeChecker();

    public Integer findPrime(Integer number) {
        Integer smallerPrime = findSmallerPrime(number);
        Integer greaterPrime = findLargerPrime(number);

        if (number - smallerPrime >= greaterPrime - number) {
            return greaterPrime;
        } else {
            return smallerPrime;
        }
    }

    private Integer findSmallerPrime(Integer number) {
        Integer primeNumber = 0;
        for (int i = number - 1; i > 2; i--) {
            if (primeChecker.checkNumberIsPrime(i)) {
                primeNumber = i;
                break;
            }
        }
        return primeNumber;
    }

    private Integer findLargerPrime(Integer number) {
        Integer primeNumber = 0;
        for (int i = number + 1; i < Integer.MAX_VALUE; i++) {
            if (primeChecker.checkNumberIsPrime(i)) {
                primeNumber = i;
                break;
            }
        }
        return primeNumber;
    }
}
