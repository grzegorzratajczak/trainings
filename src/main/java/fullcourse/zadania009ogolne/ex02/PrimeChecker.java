package fullcourse.zadania009ogolne.ex02;

public class PrimeChecker {

    public boolean checkNumberIsPrime(Integer number) {
        boolean isPrime = true;

        for (int i = 2; i < number; i++) {
            if (number % i == 0) {
                isPrime = false;
                break;
            }
        }
        return isPrime;
    }
}
