package fullcourse.zadania009ogolne.ex03;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@ToString
public class Contact {

    private String name;
    private String surName;
    private String phoneNumber;
    private String eMail;
//
//    public Contact(String name, String surName, String phoneNumber, String eMail) {
//        this.name = name;
//        this.surName = surName;
//        this.phoneNumber = phoneNumber;
//        this.eMail = eMail;
//    }

//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getSurName() {
//        return surName;
//    }
//
//    public void setSurName(String surName) {
//        this.surName = surName;
//    }
//
//    public String getPhoneNumber() {
//        return phoneNumber;
//    }
//
//    public void setPhoneNumber(String phoneNumber) {
//        this.phoneNumber = phoneNumber;
//    }
//
//    public String geteMail() {
//        return eMail;
//    }
//
//    public void seteMail(String eMail) {
//        this.eMail = eMail;
//    }
}
