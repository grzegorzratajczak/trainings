package fullcourse.zadania009ogolne.ex03;

import lombok.ToString;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ListContact {

    List<Contact> contactList = new ArrayList<>();

    public void createContactList() {
        contactList.clear();
        contactList.add(new Contact("Jan", "Kowalski", "608-609-009", "jankowal@gm.com"));
        contactList.add(new Contact("Sebastian", "Witko", "754-473-976", "seba.wito@onet.pl"));
        contactList.add(new Contact("Janina", "Kowalska", "608-609-009", "janinakowal@gm.com"));
        contactList.add(new Contact("Mateusz", "Klon", "555-666-779", "klon@gm.com"));
        contactList.add(new Contact("Witek", "Stefcio", "456-456-353", "stefo@wp.pl"));
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(new File("/home/grzex/Pulpit/contactlist.json"), contactList);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadContactListFromFile() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            contactList = mapper.readValue(new File("/home/grzex/Pulpit/contactlist.json"), new TypeReference<List<Contact>>() {
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        String string = "";
        for (Contact c : contactList) {
            string += c;
            string += "\n\r";
        }
        return string;
    }
}
