package fullcourse.zadania009ogolne.ex03;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class Ex03_controler {

    Finder finder = new Finder();
    ListContact listContact = new ListContact();

    @FXML
    Button buttonCreateList;

    public void clickCreateList(ActionEvent actionEvent) {
        listContact.createContactList();
        labInfo.setText("Powołałem do 'życia' listę i plik kontaktów");
    }

    @FXML
    Button buttonLoadList;

    public void clickLoadList(ActionEvent actionEvent) {
        listContact.loadContactListFromFile();
        labInfo.setText("Wczytałem listę kontaktów");
    }

    @FXML
    Button buttonShowList;

    public void clickOnButtonShowList(ActionEvent actionEvent) {
        labInfo.setText(listContact.toString());
    }

    @FXML
    Button buttonFunctionSearch;

    @FXML
    TextField textFieldConsole;

    @FXML
    Label labInfo;

    public void clickButtonFunctionSearch(ActionEvent actionEvent) {
        labInfo.setText(finder.findContactFromName(textFieldConsole.getText(), listContact).toString());
    }

    @FXML
    ChoiceBox choiceBoxForSearchMethod;
}
