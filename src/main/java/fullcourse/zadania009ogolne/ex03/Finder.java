package fullcourse.zadania009ogolne.ex03;

import java.util.ArrayList;
import java.util.List;

public class Finder {

    public List<Contact> findContactFromName(String string, ListContact listContact) {
        List<Contact> list = new ArrayList<>();
        for (Contact c : listContact.contactList) {
            if (c.getName().equals(string)) {
                list.add(c);
            }
        }
        return list;
    }

    public List<Contact> findContactFromSurName(String string, ListContact listContact) {
        List<Contact> list = new ArrayList<>();
        for (Contact c : listContact.contactList) {
            if (c.getSurName().equals(string)) {
                list.add(c);
            }
        }
        return list;
    }

    public List<Contact> findContactFromPhoneNumber(String string, ListContact listContact) {
        List<Contact> list = new ArrayList<>();
        for (Contact c : listContact.contactList) {
            if (c.getPhoneNumber().equals(string)) {
                list.add(c);
            }
        }
        return list;
    }

    public List<Contact> findContactFromEMail(String string, ListContact listContact) {
        List<Contact> list = new ArrayList<>();
        for (Contact c : listContact.contactList) {
            if (c.getEMail().equals(string)) {
                list.add(c);
            }
        }
        return list;
    }

}
