
package fullcourse.zadania009ogolne.ex01;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.Button;


public class Start_ex01 extends Application {

    NumberConverter numberConverter = new NumberConverter();
    String stringToShow = "";

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.show();
        BorderPane layout = new BorderPane();
        Scene scene = new Scene(layout);
        TextField textField = new TextField();
        Label label = new Label("Tutaj podam cyfrę słownie");

        primaryStage.setScene(scene);

        Button button = new Button("Słownie");
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                stringToShow = numberConverter.convertToString(Integer.parseInt(textField.getText()));
                label.setText(stringToShow);
            }
        });

        VBox vBox = new VBox();
        vBox.getChildren().addAll(textField, button, label);
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(15);
        layout.setCenter(vBox);
    }
}

