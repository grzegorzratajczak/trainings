package fullcourse.zadania02pdf.task1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj cyfrę do sprawdzenia: ");

        String valueEnteredByUser = scanner.nextLine();

        RegexValidator regexValidator = new RegexValidator();
        Boolean numberIsValid = regexValidator.checkNumber(valueEnteredByUser);
        System.out.println(String.format("Wprowadzona liczba %s%s jest poprawna", valueEnteredByUser, numberIsValid ? " " : " nie"));

        if (numberIsValid) {
            Integer parser = Integer.parseInt(valueEnteredByUser);
            if (parser % 2 == 0) {
                System.out.println("Liczba parzysta");
            } else
                System.out.println("Liczba nie parzysta");
        }

    }
}