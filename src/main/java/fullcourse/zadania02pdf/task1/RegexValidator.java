package fullcourse.zadania02pdf.task1;

import java.util.regex.Pattern;

public class RegexValidator {

    public Boolean checkNumber(String number){
        Pattern pattern = Pattern.compile("\\d+");
        Boolean bool = pattern.matcher(number).matches();
        return bool;
    }

}
