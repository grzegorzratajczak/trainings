package fullcourse.zadania02pdf.task8;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj numer Faktury w formacie FV/1xxx/xx/xxxx : ");
        String potentialInvoiceNumber = scanner.nextLine();

        InvoiceValidator invoiceValidator = new InvoiceValidator();
        Boolean invoiceNumberIsValid = invoiceValidator.checkInvoiceNumber(potentialInvoiceNumber);
        System.out.println(String.format("Podany numer faktury %s%s jest poprawny", potentialInvoiceNumber, invoiceNumberIsValid ? "" : " nie"));
    }
}
