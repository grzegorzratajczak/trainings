package fullcourse.zadania02pdf.task8;

import java.util.regex.Pattern;

public class InvoiceValidator {

    public Boolean checkInvoiceNumber(String valueFromUser){
        Pattern pattern = Pattern.compile("^FV/1[0-9]{3}/[0-9]{2}/2018$"); //in future implement number of months
        Boolean bool = pattern.matcher(valueFromUser).matches();
        return bool;

    }

}
