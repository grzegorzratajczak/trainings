package fullcourse.zadania02pdf.task9;

public class Validator {

    public Integer checkNumberOfWords(String toCheck) {
        String tableOfWords[];
        tableOfWords = toCheck.split(" ");
        Integer numberOfWords = tableOfWords.length;
        return numberOfWords;
    }

    public Integer checkNumberOfCharts(String toCheck) {
        char tableOfCharts[];
        tableOfCharts = toCheck.toCharArray();
        Integer numberOfCharts = tableOfCharts.length;
        return numberOfCharts;
    }

    public Double averageLengthOfWordsInInputString(String toCheck) {
        String tableOfWords[];
        tableOfWords = toCheck.split(" ");
        int i = 0;
        int sumOfCharts = 0;
        while (i < tableOfWords.length) {
            sumOfCharts = sumOfCharts + tableOfWords[i].length();
            i++;
        }
        System.out.println("Łączna liczba znaków: " + sumOfCharts);
        Double averageLength = sumOfCharts / (double) tableOfWords.length;
        return averageLength;
    }

    public Integer longestWord(String toCheck) {
        String tableOfWords[];
        tableOfWords = toCheck.split(" ");
        int i = 0;
        Integer numberOfChartsInLongestWord = 0;
        while (i < tableOfWords.length) {
            while (numberOfChartsInLongestWord < tableOfWords[i].length()) {
                numberOfChartsInLongestWord = tableOfWords[i].length();
            }
            i++;
        }
        return numberOfChartsInLongestWord;
    }

    public Integer shortestWord(String toCheck) {
        String tableOfWords[];
        tableOfWords = toCheck.split(" ");
        int i = 0;
        Integer numberOfChartsInShortestWord = 99999;
        while (i < tableOfWords.length) {
            while(numberOfChartsInShortestWord > tableOfWords[i].length()){
                numberOfChartsInShortestWord = tableOfWords[i].length();
            }
            i++;
        }
        return numberOfChartsInShortestWord;
    }
}
