package fullcourse.zadania02pdf.task9;

public class Main {
    public static void main(String[] args) {

        String toValidate = "Drogi Marszałku, Wysoka Izbo. PKB rośnie. " +
                "Z pełną odpowiedzialnością mogę stwierdzić iż realizacja określonych zadań stanowionych przez organizację." +
                "Dalszy rozwój jest ważne zadanie w większym stopniu tworzenie odpowiednich warunków aktywizacji." +
                "Często niezauważanym szczegółem jest to, że zakres i rozwijanie struktur pociąga za najważniejszy punkt naszych działań obierzemy praktykę, " +
                "nie zaś teorię, okazuje się jasne.";

//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Podaj wyrażenie do sprawdzenia: ");
//        String toValidate = scanner.nextLine();

        Validator validator = new Validator();
        Integer numberOfWords = validator.checkNumberOfWords(toValidate);
        Integer numberOfCharts = validator.checkNumberOfCharts(toValidate);
        System.out.println("Ilość słów: " + numberOfWords);
        System.out.println("Ilość znaków: " + numberOfCharts);

        Double averageChartsNumber = validator.averageLengthOfWordsInInputString(toValidate);
        System.out.println("Średnia ilość znaków: " + averageChartsNumber);

        Integer chartsNumberInLongestWord = validator.longestWord(toValidate);
        System.out.println("Najdłuższe słowo ma: " + chartsNumberInLongestWord + " znaków");

        Integer chartsNumberInShortestWord = validator.shortestWord(toValidate);
        System.out.println("Najkrótsze słowo ma: " + chartsNumberInShortestWord + " znaków");
    }
}
