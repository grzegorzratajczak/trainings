package fullcourse.zadania02pdf.task6;

import java.util.regex.Pattern;

public class SerialNumberValidator {

    public Boolean serialNumberCheck(String codeToValidate){

        Pattern pattern = Pattern.compile("^[A-Z]{3}[0-9]{5}[a-z]{1}[A-Z]{1}$");
        Boolean bool = pattern.matcher(codeToValidate).matches();
        return bool;

    }
}
