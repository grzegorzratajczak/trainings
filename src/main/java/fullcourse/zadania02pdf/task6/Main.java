package fullcourse.zadania02pdf.task6;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj numer seryjny: ");
        String potentialSerialNumber = scanner.nextLine();

        SerialNumberValidator serialNumberValidator = new SerialNumberValidator();
        Boolean serialNumberIsValid = serialNumberValidator.serialNumberCheck(potentialSerialNumber);
        System.out.println(String.format("Podany numer seryjny %s%s jest poprawny", potentialSerialNumber, serialNumberIsValid ? "" : " nie"));
    }
}
