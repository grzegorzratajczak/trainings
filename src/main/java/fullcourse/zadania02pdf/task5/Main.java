package fullcourse.zadania02pdf.task5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj datę: ");
        String potentialDate = scanner.nextLine();

        DateValidator dateValidator = new DateValidator();
        Boolean isDateValid = dateValidator.checkDate(potentialDate);
        System.out.println(String.format("Podana data %s%s jest poprawna", potentialDate, isDateValid ? "" : " nie"));

    }
}
