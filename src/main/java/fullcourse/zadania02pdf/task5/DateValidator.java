package fullcourse.zadania02pdf.task5;

import java.util.regex.Pattern;

public class DateValidator {

    public Boolean checkDate(String valueFromUser){
        Pattern pattern = Pattern.compile("^\\d{2}.\\d{2}.\\d{4}$");
        boolean bool = pattern.matcher(valueFromUser).matches();
        return bool;
    }
}
