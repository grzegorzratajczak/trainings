package fullcourse.zadania02pdf.task7;

import java.util.regex.Pattern;

public class SerialNumberValidator {

    public Boolean checkSerialNumber(String valueFromUser){

        Pattern pattern = Pattern.compile("^[a-zA-Z0-9]{5}-[a-zA-Z0-9]{5}-[a-zA-Z0-9]{5}-[a-zA-Z0-9]{5}-[a-zA-Z0-9]{5}");
        Boolean bool = pattern.matcher(valueFromUser).matches();
        return bool;
    }
}
