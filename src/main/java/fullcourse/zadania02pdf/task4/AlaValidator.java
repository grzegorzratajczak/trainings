package fullcourse.zadania02pdf.task4;

import java.util.regex.Pattern;

public class AlaValidator {

    public Boolean alaIsValidCheck(String fromUser){
        Pattern pattern = Pattern.compile("ala");
        boolean bool = pattern.matcher(fromUser).matches();
        return bool;

    }
}
