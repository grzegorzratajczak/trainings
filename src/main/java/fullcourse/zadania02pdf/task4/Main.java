package fullcourse.zadania02pdf.task4;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        System.out.println("Podaj sekwencję: ");
        Scanner scanner = new Scanner(System.in);
        String valueFromUser = scanner.nextLine();

        AlaValidator alaValidator = new AlaValidator();
        Boolean alaIsValue = alaValidator.alaIsValidCheck(valueFromUser);
        System.out.println(String.format("Wprowadzone stwierdzenie %s%s jest ala", valueFromUser, alaIsValue ? "" : " nie"));
    }
}
