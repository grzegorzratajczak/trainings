package fullcourse.zadania02pdf.task10;

import java.util.regex.Pattern;

public class Validator {

    public Boolean itIsPesel(String toCheck) {
        Pattern pattern = Pattern.compile("^[0-9]{11}$");
        Boolean bool = pattern.matcher(toCheck).matches();
        return bool;
    }

    public String sex(String pesel) {
        String tableOfChars[];
        String sex;
        tableOfChars = pesel.split("");
        Pattern pattern = Pattern.compile("^0|2|4|6|8$");
        if (pattern.matcher(tableOfChars[9]).matches()) {
            sex = "F";
        } else {
            sex = "M";
        }
        return sex;
    }

    public String dayOfBirthday(String pesel) {
        String tableOfChars[];
        tableOfChars = pesel.split("");
        String day = tableOfChars[4] + tableOfChars[5];
        return day;

    }

    public String yearOfBirthday(String pesel) {
        String tableOfChars[];
        tableOfChars = pesel.split("");
        String year = null;
        if (tableOfChars[2].equals("8")) {
            year = "18";
        } else if (tableOfChars[2].equals("9")) {
            year = "18";
        } else if (tableOfChars[2].equals("0")) {
            year = "19";
        } else if (tableOfChars[2].equals("1")) {
            year = "19";
        } else if (tableOfChars[2].equals("2")) {
            year = "20";
        } else if (tableOfChars[2].equals("3")) {
            year = "20";
        } else if (tableOfChars[2].equals("4")) {
            year = "21";
        } else if (tableOfChars[2].equals("5")) {
            year = "21";
        } else if (tableOfChars[2].equals("6")) {
            year = "22";
        } else if (tableOfChars[2].equals("7")) {
            year = "22";
        }
        String finalyear = year + tableOfChars[0] + tableOfChars[1];
        return finalyear;
    }

    public String monthOfBirthday(String pesel) {
        String tableOfChars[];
        tableOfChars = pesel.split("");
        String month = null;
        Pattern pattern = Pattern.compile("^[02468]$");
        boolean boolForPattern = pattern.matcher(tableOfChars[2]).matches();
        if (tableOfChars[3].equals("0")) {
            month = "październik";
        } else if (tableOfChars[3].equals("3")) {
            month = "marzec";
        } else if (tableOfChars[3].equals("4")) {
            month = "kwiecień";
        } else if (tableOfChars[3].equals("5")) {
            month = "maj";
        } else if (tableOfChars[3].equals("6")) {
            month = "czerwiec";
        } else if (tableOfChars[3].equals("7")) {
            month = "lipiec";
        } else if (tableOfChars[3].equals("8")) {
            month = "sierpień";
        } else if (tableOfChars[3].equals("9")) {
            month = "wrzesień";
        } else if (tableOfChars[3].equals("1")) {
            if (boolForPattern) {
                month = "styczeń";
            } else {
                month = "listopad";
            }
        } else if (tableOfChars[3].equals("2")) {
            if (boolForPattern) {
                month = "luty";
            } else {
                month = "grudzień";
            }
        }
        return month;
    }

}
