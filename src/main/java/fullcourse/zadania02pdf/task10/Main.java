package fullcourse.zadania02pdf.task10;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj numer PESEL: ");
        String peselFromUser = scanner.nextLine();

        Validator validator = new Validator();
        Boolean itIsPesel = validator.itIsPesel(peselFromUser);

        if (itIsPesel) {
            System.out.println("To jest poprawny PESEL");
        } else {
            System.out.println("To nie jest poprawny numer PESEL");
        }

        String sex = validator.sex(peselFromUser);
        System.out.println("Płeć: " + sex);

        String dayOfBirthday = validator.dayOfBirthday(peselFromUser);
        System.out.println("Dzień urodzin: " + dayOfBirthday);

        String yearOfBirthday = validator.yearOfBirthday(peselFromUser);
        System.out.println("Rok urodzin to: " + yearOfBirthday);

        String monthOfBirthday = validator.monthOfBirthday(peselFromUser);
        System.out.println("Miesiąc urodzenia to: " + monthOfBirthday);
    }
}
