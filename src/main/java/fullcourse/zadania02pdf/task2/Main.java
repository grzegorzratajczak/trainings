package fullcourse.zadania02pdf.task2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj kod do sprawdzenia: ");

        String valueEnteredByUser = scanner.nextLine();

        AddressCodeValidator addressCodeValidator = new AddressCodeValidator();
        Boolean addressIsValid = addressCodeValidator.checkAddressCode(valueEnteredByUser);
        System.out.println(String.format("Wprowadzony kod %s%s jest poprawny", valueEnteredByUser, addressIsValid ? "" : " nie"));

    }
}
