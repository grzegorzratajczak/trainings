package fullcourse.zadania02pdf.task2;

import java.util.regex.Pattern;

public class AddressCodeValidator {

    public Boolean checkAddressCode(String potentialAddressCode){
        Pattern pattern = Pattern.compile("^\\d{2}-?\\d{3}$");
        boolean bool = pattern.matcher(potentialAddressCode).matches();
        return bool;
    }

}
