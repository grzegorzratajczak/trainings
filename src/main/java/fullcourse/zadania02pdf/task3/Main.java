package fullcourse.zadania02pdf.task3;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj nowy Login: ");

        String loginEnteredByuser = scanner.nextLine();

        LoginValidator loginValidator = new LoginValidator();
        Boolean loginIsValid = loginValidator.loginValidate(loginEnteredByuser);
        System.out.println(String.format("Podany login %s%s jest poprawny ", loginEnteredByuser, loginIsValid ? "" : " nie"));
    }
}
