package fullcourse.zadania02pdf.task3;

import java.util.regex.Pattern;

public class LoginValidator {

    public Boolean loginValidate(String potentialLogin) {
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9]{8,16}$");
        boolean bool = pattern.matcher(potentialLogin).matches();
        return bool;
    }
}
