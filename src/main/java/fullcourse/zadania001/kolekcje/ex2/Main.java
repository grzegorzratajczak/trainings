package fullcourse.zadania001.kolekcje.ex2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Random random = new Random();
        List<Integer> listOfNumbers = new ArrayList<>();

        for(int i = 0; i<10 ; i++){
            listOfNumbers.add(random.nextInt());
        }

        System.out.println(listOfNumbers);

    }
}