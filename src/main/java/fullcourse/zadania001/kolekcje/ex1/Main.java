package fullcourse.zadania001.kolekcje.ex1;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Set<String> listOfStrings = new TreeSet<>();

        for (int i = 0; i < 5; i++) {
            System.out.println("Podaj imię numer " + (i + 1) + ":");
            listOfStrings.add(scanner.nextLine());
        }

        System.out.println(listOfStrings);
    }
}
