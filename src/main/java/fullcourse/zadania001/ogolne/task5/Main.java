package fullcourse.zadania001.ogolne.task5;

import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Random random = new Random();
        int[] randomNumbers = random.ints(100, 0, 201).toArray();

        HashMap<Integer, Integer> result = new HashMap<>();

        for(int i: randomNumbers){
            if (result.containsKey(i)) result.put(i, result.get(i)+1);
            else result.put(i, 1);
        }
        for (int i: result.keySet()) System.out.println(i + ":" + result.get(i));


        List<Integer> top5Numbers = result.entrySet()
                .stream()
                .sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
                .map(entry -> entry.getKey())
                .limit(5)
                .collect(Collectors.toList());
        System.out.println(top5Numbers);
    }
}
