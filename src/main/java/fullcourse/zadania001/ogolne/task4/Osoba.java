package fullcourse.zadania001.ogolne.task4;

public class Osoba {
    private String imie;
    private String nazwisko;
    private String pesel;

    public Osoba(String imie, String nazwisko, String pesel) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.pesel = pesel;
    }

    @Override
    public String toString(){
        return String.format("Imię: %s Nazwisko: %s PESEL: %s",imie,nazwisko,pesel);
    }
}
