package fullcourse.zadania001.ogolne.task2;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        ArrayList<Integer> listOfNumbers = new ArrayList();
        Scanner scanner = new Scanner(System.in);


        String valueFromUser;

        Validator validator = new Validator();

        do {
            System.out.println("Podaj liczbę: ");
            valueFromUser = scanner.nextLine();

            if (validator.itIsNumber(valueFromUser)) {
                listOfNumbers.add(Integer.valueOf(valueFromUser));
                Integer sum = 0;
                for (Integer d : listOfNumbers) {
                    sum += d;
                }
                System.out.println("Suma liczb: " + sum);
            }
        }while(validator.itIsNumber(valueFromUser));

    }
}
