package fullcourse.zadania001.ogolne.task2;

import java.util.regex.Pattern;

public class Validator {

    boolean itIsNumber (String valueFromUser){
        Pattern pattern = Pattern.compile("^\\d+$");
        boolean bool = pattern.matcher(valueFromUser).matches();
        return bool;
    }
}
