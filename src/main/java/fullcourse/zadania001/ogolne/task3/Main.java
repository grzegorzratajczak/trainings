package fullcourse.zadania001.ogolne.task3;


import java.util.ArrayList;
import java.util.Collections;

public class Main {
    public static void main(String[] args) {

        ArrayList<Integer> list = new ArrayList<>();
        for (int i=1; i<100; i++) {
            list.add(new Integer(i));
        }
        Collections.shuffle(list);
        for (int i=0; i<20; i++) {
            System.out.println(list.get(i));
        }
    }
}
