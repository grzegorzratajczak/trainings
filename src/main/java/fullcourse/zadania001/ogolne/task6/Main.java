package fullcourse.zadania001.ogolne.task6;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {

        Random random = new Random();
        int[] randomNumbers = random.ints(1000, 1000, 2001).toArray();

//        new ArrayList<Integer>() {{ for (int i : randomNumbers) add(i); }}

        List<Integer> numberList = Arrays.stream(randomNumbers).boxed().collect(Collectors.toList());

        Integer topNumber = Collections.max(numberList);
        System.out.println("Najwyższa wartość: " + topNumber);

        Integer smalestNumber = Collections.min(numberList);
        System.out.println("Najmniejsza wartość: " + smalestNumber);

    }
}
