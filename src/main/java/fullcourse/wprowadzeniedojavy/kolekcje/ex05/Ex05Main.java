package fullcourse.wprowadzeniedojavy.kolekcje.ex05;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ex05Main {

    public static void main(String[] args) {

        List<String> listOfNames = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 5; i++) {
            System.out.println("Podaj imię numer " + (i + 1) + ": ");
            listOfNames.add(scanner.nextLine());
        }

        int j = 0;
        while (j < listOfNames.size()) {
            System.out.println("Imię numer " + (j + 1) + " to " + listOfNames.get(j));
            j++;
        }

    }
}
