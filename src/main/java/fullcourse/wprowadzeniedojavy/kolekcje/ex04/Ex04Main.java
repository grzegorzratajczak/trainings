package fullcourse.wprowadzeniedojavy.kolekcje.ex04;

import java.util.ArrayList;
import java.util.List;

public class Ex04Main {

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();

        list.add("Jan");
        list.add("Piotr");
        list.add("Paweł");
        list.add("Gabriel");
        list.add("Mateusz");

        for (int i = 0; i < list.size(); i++) {
            System.out.println("Imię " + (i + 1) + " to: " + list.get(i));
        }

        for (int i = list.size() - 1; i >= 0; i--) {
            System.out.println("Imię " + (i + 1) + " to: " + list.get(i));
        }

    }
}
