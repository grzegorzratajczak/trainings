package fullcourse.wprowadzeniedojavy.kolekcje.ex06;

import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;
import java.util.Scanner;

public class Ex06Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Queue<Integer> numberList = new PriorityQueue<>();

        int counter;
        System.out.println("Podaj ilość liczb do wylosowania: ");
        counter = scanner.nextInt();

        Random random = new Random();

        while (counter > 0) {
            numberList.add(random.nextInt(100));
            counter--;
        }

        while (numberList.size() > 0) {
            System.out.println(numberList.remove());
        }

    }
}
