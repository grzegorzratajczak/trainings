package fullcourse.wprowadzeniedojavy.kolekcje.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ex03Main {

    public static void main(String[] args) {

        List<Double> list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        for (int i = 0; i < 10; i++) {
            System.out.println("Podaj liczbę " + (i + 1) + ": ");
            list.add(scanner.nextDouble());
        }

        for (int i = 0; i < list.size(); i++) {
            System.out.println((i + 1) + " element listy = " + list.get(i));
        }

        System.out.println("Suma pierwszego i ostatniego: " + (list.get(0) + list.get(list.size() - 1)));
        System.out.println("Iloczyn pierwszego i ostatniego: " + (list.get(0) * list.get(list.size() - 1)));
        System.out.println("Iloraz pierwszego i przedostatniego: " + (list.get(0) / list.get(list.size() - 2)));


    }
}
