package fullcourse.wprowadzeniedojavy.kolekcje.ex01;

public class Ex01Main {

    public static void main(String[] args) {

        int[] array = new int[10];

        ArrayOperations.makeIncrementationFrom1Array(array);

        System.out.println("Długość tablicy: " + array.length);

        ArrayOperations.ShowOnConsole(array);

        ArrayOperations.multiplyAllValuesBy2(array);

        ArrayOperations.ShowOnConsole(array);
    }
}
