package fullcourse.wprowadzeniedojavy.kolekcje.ex01;

public class ArrayOperations {

    public static int[] makeIncrementationFrom1Array(int[] array){
        for (int i = 0; i < array.length; i++) {
            array[i] = i + 1;
        }
        return array;
    }

    public static void ShowOnConsole(int[] array){
        for (int i = 0; i < array.length; i++) {
            System.out.println("Miejsce w tablicy[" + i + "] = " + array[i]);
        }
    }

    public static int[] multiplyAllValuesBy2(int[] array){
        System.out.println("Mnożę wszystkie wartości w tablicy przez 2");
        for (int i = 0; i < array.length; i++) {
            array[i] = array[i] * 2;
        }
        return array;
    }

}
