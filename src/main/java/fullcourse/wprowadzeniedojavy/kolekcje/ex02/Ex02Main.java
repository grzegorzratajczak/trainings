package fullcourse.wprowadzeniedojavy.kolekcje.ex02;

import java.util.ArrayList;
import java.util.List;

public class Ex02Main {

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();

        list.add(2);
        list.add(5);
        list.add(7);

        System.out.println(list.get(0));

        System.out.println(list.get(list.size() - 1));
    }
}
