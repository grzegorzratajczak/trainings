package fullcourse.designpattern.singleton;

public class LazySingletone {

    private static LazySingletone instance;

    public synchronized static LazySingletone getInstance() {
        if (instance == null) {
            instance = new LazySingletone();
        }
        return instance;
    }

}
