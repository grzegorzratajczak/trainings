package fullcourse.designpattern.singleton;

public class Main {

    public static void main(String[] args) {
        Singleton01.getInstance().saveLog("message");

        System.out.println("Lazy singleton");
        System.out.println(LazySingletone.getInstance());
        System.out.println(LazySingletone.getInstance());

        System.out.println("Eager singleton");
        System.out.println(Singleton01.getInstance());
        System.out.println(Singleton01.getInstance());

        System.out.println(LazySingletonThreadSafe.getInstance());
        System.out.println(LazySingletonThreadSafe.getInstance());

        EnumSingletone.saveLog();
    }

}
