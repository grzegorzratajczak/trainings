package fullcourse.designpattern.adapter;

public class Email {

    public boolean connect(){
        System.out.println("Connect");
        return true;
    }

    public boolean authorize(String login, String password){
        System.out.println(String.format("Login: %s, pass: %s", login, password));
        return true;
    }

    public boolean sendMessage(String msg){
        System.out.println("Send message: " + msg);
        return true;
    }

    public boolean disconnect(){
        System.out.println("Disconnect");
        return true;
    }
}
