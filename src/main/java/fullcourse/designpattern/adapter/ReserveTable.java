package fullcourse.designpattern.adapter;

import java.time.LocalDateTime;

public interface ReserveTable {

    boolean reserveTable(String person, LocalDateTime date, int numberOfPerson);

}
