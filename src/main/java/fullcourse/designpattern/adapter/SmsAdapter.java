package fullcourse.designpattern.adapter;

import java.time.LocalDateTime;

public class SmsAdapter extends BaseAdapter implements ReserveTable {

    Sms sms = new Sms();
    String telephone;
    String smsGateway;

    @Override
    public boolean reserveTable(String person, LocalDateTime date, int numberOfPerson) {
        if (sms.checkline()) {
            sms.sendSms(String.format(msg, date, numberOfPerson, person), telephone, smsGateway);
            System.out.println("SMS Message: " + String.format(msg, date, numberOfPerson, person));
            return true;
        }
        return false;
    }
}
