package fullcourse.designpattern.adapter;

public class Sms {

    public boolean checkline(){
        System.out.println("Check line...");
        return true;
    }

    public boolean sendSms(String textMessage, String number, String smsGateway){
        System.out.println("Sending SMS message");
        return true;
    }

}
