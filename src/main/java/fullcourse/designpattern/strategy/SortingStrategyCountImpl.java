package fullcourse.designpattern.strategy;

import fullcourse.beztytulu.SortCount;

public class SortingStrategyCountImpl extends SortCount implements SortingStrategy {

    SortCount sortCount;

    @Override
    public int[] makesort(int[] array) {
        return sortCount.sortByCount(array);
    }
}
