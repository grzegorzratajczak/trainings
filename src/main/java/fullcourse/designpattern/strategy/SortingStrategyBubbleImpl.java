package fullcourse.designpattern.strategy;

import fullcourse.beztytulu.SortBubble;

public class SortingStrategyBubbleImpl extends SortBubble implements SortingStrategy {

    SortBubble sortBubble;

    @Override
    public int[] makesort(int[] array) {
        return sortBubble.bubbleSort(array);
    }
}
