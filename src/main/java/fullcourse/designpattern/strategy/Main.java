package fullcourse.designpattern.strategy;

/*
Program to compare methods times to sorting array
 */

public class Main {
    public static void main(String[] args) {

        int arrayToTimeTest[] = {2, 542, 345, 534, 432, 543, 8786, 3686, 3, 4, 6, 1, 2532, 462, 54745, 345, 2, 1, 7, 6, 5, 34, 45, 4, 53543, 34, 23, 64, 3, 463};
        SortingStrategy sortBubble = new SortingStrategyBubbleImpl();
        SortingStrategy sortCount = new SortingStrategyCountImpl();
        SortingTimeCounter sortTime = new SortingTimeCounter();

        int sortBubbleTime = sortTime.calculate(arrayToTimeTest, sortBubble);
        int sortCountTime = sortTime.calculate(arrayToTimeTest, sortCount);

        System.out.println("Sortowanie bąbelkowe - czas: " + sortBubbleTime + "ns");
        System.out.println("Sortowanie przez zliczanie - czas: " + sortCountTime + "ns");
        System.out.println("Stosunek bubble/count= " + (sortBubbleTime / sortCountTime));
        System.out.println("Stosunek count/bubble= " + (sortCountTime / sortBubbleTime));

    }
}
