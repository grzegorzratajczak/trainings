package fullcourse.designpattern.strategy;

public interface SortingStrategy {

    int[] makesort(int[] array);

}
