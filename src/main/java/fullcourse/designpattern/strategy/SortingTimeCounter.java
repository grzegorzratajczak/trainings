package fullcourse.designpattern.strategy;

import java.time.LocalTime;

public class SortingTimeCounter {

    LocalTime startTime;
    LocalTime endTime;

    int calculate(int[] array, SortingStrategy sortingStrategy) {
        startTime = LocalTime.now();
        sortingStrategy.makesort(array);
        endTime = LocalTime.now();
        return endTime.getNano() - startTime.getNano();
    }

}
