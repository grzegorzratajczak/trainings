package fullcourse.designpattern.fabricmethod.generators;

import fullcourse.designpattern.fabricmethod.reports.DraftReport;
import fullcourse.designpattern.fabricmethod.reports.Report;

public class DraftReportGenerator implements ReportGenerator {
    @Override
    public Report generateReport(ReportData data) {
        // report generation
        return new DraftReport();
    }
}
