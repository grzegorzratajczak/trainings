package fullcourse.designpattern.fabricmethod.generators;

import fullcourse.designpattern.fabricmethod.reports.Report;

@FunctionalInterface
public interface ReportGenerator {

    Report generateReport(ReportData data);

}
