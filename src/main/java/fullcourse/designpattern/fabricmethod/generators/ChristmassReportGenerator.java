package fullcourse.designpattern.fabricmethod.generators;

import fullcourse.designpattern.fabricmethod.reports.ChristmassReport;
import fullcourse.designpattern.fabricmethod.reports.Report;

public class ChristmassReportGenerator implements ReportGenerator {
    @Override
    public Report generateReport(ReportData data) {
        // report generation
        return new ChristmassReport();
    }
}
