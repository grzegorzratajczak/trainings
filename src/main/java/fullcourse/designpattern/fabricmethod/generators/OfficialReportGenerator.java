package fullcourse.designpattern.fabricmethod.generators;

import fullcourse.designpattern.fabricmethod.reports.OfficialReport;
import fullcourse.designpattern.fabricmethod.reports.Report;

public class OfficialReportGenerator implements ReportGenerator {
    @Override
    public Report generateReport(ReportData data) {
        // Report generation process
        return new OfficialReport();
    }
}
