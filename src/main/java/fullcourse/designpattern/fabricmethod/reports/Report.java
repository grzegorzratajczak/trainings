package fullcourse.designpattern.fabricmethod.reports;

import fullcourse.designpattern.fabricmethod.Page;

public abstract class Report {

    private String header;
    private String footer;
    private String index;
    private String tableOfContent;
    Page[] pages;
    public abstract void printReport();

}
