package fullcourse.designpattern.fabricmethod.reports;

public class OfficialReport extends Report {

    @Override
    public void printReport() {
        System.out.println("Printing official report");
    }
}
