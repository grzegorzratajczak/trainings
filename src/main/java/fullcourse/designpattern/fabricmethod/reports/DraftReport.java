package fullcourse.designpattern.fabricmethod.reports;

public class DraftReport extends Report {

    @Override
    public void printReport() {
        System.out.println("Printing draft report");
    }
}
