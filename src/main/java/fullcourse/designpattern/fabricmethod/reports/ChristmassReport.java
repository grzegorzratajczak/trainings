package fullcourse.designpattern.fabricmethod.reports;

public class ChristmassReport extends Report {

    @Override
    public void printReport() {
        System.out.println("Printing Christmass report");
    }
}
