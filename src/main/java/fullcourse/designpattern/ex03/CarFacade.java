package fullcourse.designpattern.ex03;

public class CarFacade {

    Car car = new Car();

    private boolean isCarReadyToStartEngine() {
        if (car.checkAbsSystem() &&
                car.checkAirBagSystem() &&
                car.checkBatter() &&
                car.checkEspSystem() &&
                car.checkFuel() &&
                car.checkOil()) {
            return true;
        }
        return false;
    }

    public void startCarEngine() {
        if (isCarReadyToStartEngine()) {
            car.startEngine();
        } else {
            System.out.println("Nie udało się włączyć silnika");
        }
    }

}
