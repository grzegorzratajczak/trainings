package fullcourse.designpattern.ex03;

public class Main {
    public static void main(String[] args) {

        CarFacade car = new CarFacade();

        car.startCarEngine();
    }
}
