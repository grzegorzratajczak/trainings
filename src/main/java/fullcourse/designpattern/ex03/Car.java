package fullcourse.designpattern.ex03;

public class Car {

    public boolean checkBatter() {
        System.out.println("Akumulator - sprawny");
        return true;
    }

    public boolean checkFuel() {
        System.out.println("Paliwo - jest ;)");
        return true;
    }

    public boolean checkAbsSystem() {
        System.out.println("Abs - sprawny");
        return true;
    }

    public boolean checkOil() {
        System.out.println("Olej - sprawdzony");
        return true;
    }

    public boolean checkEspSystem() {
        System.out.println("Eps - sprawne");
        return true;
    }

    public boolean checkAirBagSystem() {
        System.out.println("Poduszki powietrzne - sprawne");
        return true;
    }

    public boolean startEngine() {
        System.out.println("Silnik został uruchomiony");
        return true;
    }

}
