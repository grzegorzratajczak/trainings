package fullcourse.zadania014rekurencja.ex03;

public class Ex03Start {

    /**
     * Method for Factorial Result from number
     *
     * @param nrFactorial
     * @return Factorial result
     */
    public static long giveMeFactorial(long nrFactorial) {

        if (nrFactorial == 0) {
            return 1;
        } else {
            return nrFactorial * giveMeFactorial(nrFactorial - 1);
        }
    }

    public static long giveMeFactorialLoop(long nrFactorial) {
        if (nrFactorial == 0)
            return 1;
        long result = 1;
        for (int i = 1; i <= nrFactorial; i++) {
            result *= i;
        }
        return result;
    }

    public static void main(String[] args) {

        long start;
        long end;

        System.out.println("0!: " + giveMeFactorial(0));
        System.out.println("loop 0!: " + giveMeFactorialLoop(0));
        System.out.println("1!: " + giveMeFactorial(1));
        System.out.println("loop 1!: " + giveMeFactorialLoop(1));
        System.out.println("5!: " + giveMeFactorial(5));
        System.out.println("loop 5!: " + giveMeFactorialLoop(5));

        start = System.nanoTime();
        System.out.println("25!: " + giveMeFactorial(25));
        end= System.nanoTime();
        System.out.println("Czas dla 25 silnia - rekurencja: " + (end - start) + "ns");

        start = System.nanoTime();
        System.out.println("loop 25!: " + giveMeFactorialLoop(25));
        end= System.nanoTime();
        System.out.println("Czas dla 25 silnia - pętla: " + (end - start) + "ns");
    }
}
