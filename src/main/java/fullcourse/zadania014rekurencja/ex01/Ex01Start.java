package fullcourse.zadania014rekurencja.ex01;

import java.util.Scanner;

public class Ex01Start {

    public static String wordReverse(String str) {
        if ((null == str) || (str.length() <= 1)) {
            return str;
        }
        return wordReverse(str.substring(1)) + str.charAt(0);
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj do zamiany: ");
        String fromScan = scanner.nextLine();
        String toShow = wordReverse(fromScan);
        System.out.println(toShow);

    }
}
