package fullcourse.zadania006.ex01;

import java.util.ArrayList;
import java.util.List;

public class Margherita implements Pizza, Ingredients {

    private PizzaDough pizzaDough;
    List<String> ingredients = new ArrayList<>();

    @Override
    public List<String> getIngredients(){
        return ingredients;
    }

    public Margherita(List<String> ingredients, PizzaDough pizzaDough) {
        this.ingredients = ingredients;
        this.pizzaDough = pizzaDough;
    }

    public Margherita() {
    }

    @Override
    public void preparePizza() {
        System.out.println("Zaczynam robić Margerite");
        pizzaDough.preparePizzaDough();
        System.out.println("Zrobiłem pizzę Margherita");

    }

}
