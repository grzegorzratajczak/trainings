package fullcourse.zadania006.ex01;

import java.util.ArrayList;
import java.util.List;

public class Hawaiian implements Pizza, Ingredients {

    private PizzaDough pizzaDough;
    List<String> ingredients = new ArrayList<>();

    @Override
    public List<String> getIngredients(){
        return ingredients;
    }

    public Hawaiian(List<String> ingredients, PizzaDough pizzaDough) {
        this.ingredients = ingredients;
        this.pizzaDough = pizzaDough;
    }

    public Hawaiian() {
    }

    @Override
    public void preparePizza() {
        System.out.println("Zaczynam robić Hawajską");
        pizzaDough.preparePizzaDough();
        System.out.println(ingredients);
        System.out.println("Zrobiłem pizzę Hawajską");

    }

}
