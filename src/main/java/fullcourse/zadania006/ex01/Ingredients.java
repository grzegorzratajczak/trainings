package fullcourse.zadania006.ex01;

import java.util.List;

public interface Ingredients {

    List<String> getIngredients();

}
