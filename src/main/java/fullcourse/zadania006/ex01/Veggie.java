package fullcourse.zadania006.ex01;

import java.util.ArrayList;
import java.util.List;

public class Veggie implements Pizza, Ingredients {

    private PizzaDough pizzaDought;
    List<String> ingredients = new ArrayList<>();

    @Override
    public List<String> getIngredients(){
        return ingredients;
    }

    public Veggie(List<String> ingredients, PizzaDough pizzaDough) {
        this.ingredients = ingredients;
        this.pizzaDought = pizzaDough;
    }

    public Veggie() {
    }

    @Override
    public void preparePizza() {
        System.out.println("Zaczynam robić wegańską");
        pizzaDought.preparePizzaDough();
        System.out.println("Zrobiłem pizzę Wegańską");

    }

}
