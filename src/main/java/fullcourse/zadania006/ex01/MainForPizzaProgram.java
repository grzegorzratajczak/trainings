package fullcourse.zadania006.ex01;

import java.util.ArrayList;
import java.util.List;

public class MainForPizzaProgram {
    public static void main(String[] args) {

        List<String> listOfIngredientsForHawaiian = new ArrayList<String>();
        listOfIngredientsForHawaiian.add("Ser");
        listOfIngredientsForHawaiian.add("Ananas");

        Hawaiian hawajska1 = new Hawaiian(listOfIngredientsForHawaiian, new GlutenFree());
        hawajska1.preparePizza();

    }

}
