package fullcourse.zadania006.ex01;

public class GlutenFree implements PizzaDough {
    @Override
    public void preparePizzaDough() {
        System.out.println("Przygotowywanie ciasta GlutenFree");
    }
}
