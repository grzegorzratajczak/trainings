package fullcourse.zadania006.ex01;

public class StuffedCrust implements PizzaDough {
    @Override
    public void preparePizzaDough() {
        System.out.println("Przygotowywanie ciasta StuffedCrust");
    }
}
