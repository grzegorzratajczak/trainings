package fullcourse.zadania014rekurencja.ex01;

import org.junit.Test;

import static org.junit.Assert.*;

public class Ex01StartTest {

    private final Ex01Start objTest= new Ex01Start();

    @Test
    public void wordReverse() {

        assertEquals("HaNna",objTest.wordReverse("anNaH"));
        assertEquals("Grzes przez wies", objTest.wordReverse("seiw zezrp sezrG"));
        assertEquals("12345", objTest.wordReverse("54321"));

    }
}