package fullcourse.zadania014rekurencja.ex03;

import org.junit.Test;

import static org.junit.Assert.*;

public class Ex03StartTest {

    private final Ex03Start objTest = new Ex03Start();

    @Test
    public void giveMeFactorial() {
        assertEquals(1L, objTest.giveMeFactorial(0));
        assertEquals(1L, objTest.giveMeFactorial(1));
        assertEquals(1307674368000L, objTest.giveMeFactorial(15));
    }

    @Test
    public void giveMeFactorialLoop() {
        assertEquals(1L, objTest.giveMeFactorial(0));
        assertEquals(1L, objTest.giveMeFactorial(1));
        assertEquals(1307674368000L, objTest.giveMeFactorial(15));
    }
}