package fullcourse.zadania009ogolne.ex02;

import org.junit.Assert;
import org.junit.Test;

public class FindNearestPrimeTest {

    @Test
    public void findPrimeWhenFindPrimaryNumberIsLargerThenProvided(){
        FindNearestPrime findNearestPrime = new FindNearestPrime();
        Integer number = 10;
        Integer expectedPrime = 11;
        Assert.assertEquals(expectedPrime, findNearestPrime.findPrime(number));
    }

    @Test
    public void findPrimeWhenFindPrimaryNumberIsSmallerThenProvided(){
        FindNearestPrime findNearestPrime = new FindNearestPrime();
        Integer number = 8;
        Integer expectedPrime = 7;
        Assert.assertEquals(expectedPrime, findNearestPrime.findPrime(number));
    }

    @Test
    public void findPrimeWhenFindPrimaryIsBigger(){
        FindNearestPrime findNearestPrime = new FindNearestPrime();
        Integer number = 10;
        Integer expectedPrime = 11;
        Assert.assertEquals(expectedPrime, findNearestPrime.findPrime(number));
    }

}
