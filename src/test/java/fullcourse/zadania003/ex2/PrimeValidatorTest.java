package fullcourse.zadania003.ex2;

import org.junit.Assert;
import org.junit.Test;

public class PrimeValidatorTest {

    @Test
    public void testForPrimeValidator() {
        PrimeValidator validatorTest = new PrimeValidator();

        boolean isPrime = validatorTest.isPrime(0);
        Assert.assertFalse("nie przeszedl testu dla 0", isPrime);

        isPrime = validatorTest.isPrime(1);
        Assert.assertFalse("nie przeszedl testu dla 1", isPrime);

        isPrime = validatorTest.isPrime(2);
        Assert.assertTrue("nie przeszedl testu dla 2", isPrime);

        isPrime = validatorTest.isPrime(3);
        Assert.assertTrue("nie przeszedl testu dla 3", isPrime);

        isPrime = validatorTest.isPrime(4);
        Assert.assertFalse("nie przeszedl testu dla 4", isPrime);

        isPrime = validatorTest.isPrime(5);
        Assert.assertTrue("nie przeszedl testu dla 5", isPrime);

        isPrime = validatorTest.isPrime(6);
        Assert.assertFalse("nie przeszedl testu dla 6", isPrime);

        isPrime = validatorTest.isPrime(7);
        Assert.assertTrue("nie przeszedl testu dla 7", isPrime);

        isPrime = validatorTest.isPrime(8);
        Assert.assertFalse("nie przeszedl testu dla 8", isPrime);

        isPrime = validatorTest.isPrime(9);
        Assert.assertFalse("nie przeszedl testu dla 9", isPrime);

        isPrime = validatorTest.isPrime(10);
        Assert.assertFalse("nie przeszedl testu dla 10", isPrime);

        isPrime = validatorTest.isPrime(41);
        Assert.assertTrue("nie przeszedl testu dla 41", isPrime);

        isPrime = validatorTest.isPrime(42);
        Assert.assertFalse("nie przeszedl testu dla 42", isPrime);
    }
}
