package fullcourse.beztytulu;

import org.junit.Assert;
import org.junit.Test;

public class SortBubbleTest {

    @Test
    public void testForSortBubble(){

        int[] table1ForBubbleTest = new int[]{1,2,3,4,5,6,7,8,9};
        int[] table2ForBubbleTest = new int[]{5,5,3,2,3,1,4,4,6};
        int[] table3ForBubbleTest = new int[]{9,8,7,6,5,4,3,2,1};
        int[] pattern1ForBubbleTest = {1,2,3,4,5,6,7,8,9};
        int[] pattern2ForBubbleTest = {1,2,3,3,4,4,5,5,6};
        int[] pattern3ForBubbleTest = {1,2,3,4,5,6,7,8,9};

        Assert.assertArrayEquals("Nie przeszlo dla tabeli 1", pattern1ForBubbleTest, SortBubble.bubbleSort(table1ForBubbleTest));
        Assert.assertArrayEquals("Nie przeszlo dla tabeli 2", pattern2ForBubbleTest, SortBubble.bubbleSort(table2ForBubbleTest));
        Assert.assertArrayEquals("Nie przeszlo dla tabeli 3", pattern3ForBubbleTest, SortBubble.bubbleSort(table3ForBubbleTest));
    }

}
