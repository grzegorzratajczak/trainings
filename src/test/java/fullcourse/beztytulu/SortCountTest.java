package fullcourse.beztytulu;

import org.junit.Assert;
import org.junit.Test;

public class SortCountTest {

    @Test
    public void testForSortCount(){

        int[] table1ForCountTest = new int[]{1,2,3,4,5,6,7,8,9};
        int[] table2ForCountTest = new int[]{5,5,3,2,3,1,4,4,6};
        int[] table3ForCountTest = new int[]{9,8,7,6,5,4,3,2,1};
        int[] pattern1ForCountTest = {1,2,3,4,5,6,7,8,9};
        int[] pattern2ForCountTest = {1,2,3,3,4,4,5,5,6};
        int[] pattern3ForCountTest = {1,2,3,4,5,6,7,8,9};

        Assert.assertArrayEquals("Nie przeszlo dla tabeli 1", pattern1ForCountTest, SortCount.sortByCount(table1ForCountTest));
        Assert.assertArrayEquals("Nie przeszlo dla tabeli 2", pattern2ForCountTest, SortCount.sortByCount(table2ForCountTest));
        Assert.assertArrayEquals("Nie przeszlo dla tabeli 3", pattern3ForCountTest, SortCount.sortByCount(table3ForCountTest));
    }


}
