package fullcourse.wprowadzeniedojavy.kolekcje.ex01;

import org.junit.Assert;
import org.junit.Test;

public class ArrayOperationsTest {

    @Test
    public void makeIncrementationFrom1Array() {

        int[] arrayToTest1 = new int[3];
        int[] arrayToTest1Pattern = {1, 2, 3};
        int[] arrayToTest2 = new int[4];
        int[] arrayToTest2Pattern = {1, 2, 3, 4};

        Assert.assertArrayEquals("Nie przeszło dla tablicy 1", arrayToTest1Pattern, ArrayOperations.makeIncrementationFrom1Array(arrayToTest1));
        Assert.assertArrayEquals("Nie przeszło dla tablicy 2", arrayToTest2Pattern, ArrayOperations.makeIncrementationFrom1Array(arrayToTest2));
    }

    @Test
    public void multiplyAllValuesBy2() {

        int[] arrayToTest1 = {1,2,3};
        int[] arrayToTest1Pattern = {2,4,6};
        int[] arrayToTest2 = {1,2,3,4};
        int[] arrayToTest2Pattern = {2,4,6,8};

        Assert.assertArrayEquals("Nie przeszło dla tablicy 1", arrayToTest1Pattern, ArrayOperations.multiplyAllValuesBy2(arrayToTest1));
        Assert.assertArrayEquals("Nie przeszło dla tablicy 2", arrayToTest2Pattern, ArrayOperations.multiplyAllValuesBy2(arrayToTest2));
    }
}